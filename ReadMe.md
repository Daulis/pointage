# Pointage App

Bienvenue dans l'application de pointage, un système de gestion du temps pour enregistrer les heures de travail des employés.

## Configuration

### Base de données

L'application utilise MySQL comme base de données. Assurez-vous d'avoir une instance MySQL en cours d'exécution avant de démarrer l'application.

#### Configuration de la base de données

- Hôte : `mysql_server`
- Port : `3307`
- Utilisateur : `daulis`
- Mot de passe : `root`
- Nom de la base de données : `pointage`

### Installation des dépendances

Assurez-vous d'avoir [Node.js](https://nodejs.org/) installé.

## Installation et lancement

1. Clonez le dépôt :

   ```bash
   git clone https://gitlab.com/Daulis/pointage.git
   ```

2. Accédez au répertoire du projet :
   cd pointage-app

3. Installez les dépendances du projet :
   npm install

4. Construisez l'image Docker et lancez les conteneurs :
   docker-compose up --build
   L'application sera accessible à l'adresse http://localhost:4000

## Foncuionnalités

L'application offre les fonctionnalités suivantes:

- Enregistrement d'un employé
- Affichage de tous les employés
- Affichage des employés avec un filtre de date
- Enregistrement des heures de check-in et check-out
- Calcul automatique de la durée entre le check-in et le check-out

## API Endpoints et exemple d'utilisation des Endpoints

- POST /employee: Enregistre un nouvel employé.
  .
- GET /employee: Récupère la liste de tous les employés.
- GET /employee/:dateCreated:dateCreated: Récupère la liste des employés avec un filtre de date.
- POST /employee/check_in: Enregistre l'heure de check-in pour un employé.
- POST /employee/check-out: Enregistre l'heure de check-out pour un employé.

## Exemple d'utilisation des Endpoints

1. Pour enregistrer un nouvel employé

- verbe: post
- endpoint: /employee
- status 200 si OK avec un message "Employee ajout avec success" si non return status 500
- exemple donnée à insérer:
  {
  "name": "JOHN",
  "firstName":"DOE",
  "department":"RH"
  }

2. Pour Récupèrer la liste de tous les employés

- verbe: get
- endpoint: /employee
- reponse: status 200 si OK
  [
  {
  "id": 1,
  "name": "JOHN",
  "firstName":"DOE",
  "department":"RH",
  "dateCreated": "2023-12-14T21:07:58.000Z"
  },
  {
  "id": 2,
  "name": "Claude",
  "firstName":"Joe",
  "department":"IT",
  "dateCreated": "2023-12-15T21:07:58.000Z",
  }
  ]
- si non retunr status 500

3. Pour Récupèrer la liste des employés avec un filtre de date

- verbe: get
- endpoint: /employee/2023-12-15
- reponse: status 200 si OK
  [
  {
  "id": 2,
  "name": "Claude",
  "firstName":"Joe",
  "department":"IT",
  "dateCreated": "2023-12-15T21:07:58.000Z",
  }
  ]
- si non retunr status 500

4. Pour enregistrer l'heure de check-in pour un employé.

- verbe: post
- endpoint: /employee/check_in
- reponse status 200 si OK avec un message "Check-in effectué avec succès" si non return status 500
- exemple des données à insérer:
  {
  "employeeId": "1",
  "comment": "Un peux en retard"
  }

5. Pour enregistrer l'heure de check-out pour un employé.

- verbe: post
- endpoint: /employee/check_out
- reponse status 200 si OK avec un message "Check-out effectué avec succès" si non return status 500
- exemple des données à insérer:
  {
  "employeeId": "1",
  "comment": "C'est l'heure de partir"
  }
