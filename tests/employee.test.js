const chai = require('chai');
const expect = chai.expect;
const should = chai.should()
const chaiHttp = require('chai-http');
const server = require('../server');
const pool = require('../app/database/database')

chai.use(chaiHttp);

describe('Employee API TEST', () => {
  beforeEach(async () => {
    // Supprimez tous les enregistrements de la table des employés avant chaque test
    await pool.query('DELETE FROM employees');
  });
  
  describe('First test connection', () => {
  
    it('Test welcome route..', (done) => {
      chai.request(server)
      .get('/')
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        const welcomeMessage = res.body.message;
        expect(welcomeMessage).to.be.equal('Welcome');
        done();
      });
    })
  
    it('Retourner la liste de tous les employés', (done) => {
      chai.request(server)
      .get('/employee')
      .end(async (err, res) => {
        try {
          res.should.have.status(200);
          res.body.should.be.a('array');
          done();
        } catch (error) {
          done(error);
        }
      });
    })
    
    it('Devrait retourner la listes des employee par date', (done) => {
      const date = '2023-12-15 :06:10.000'
      chai.request(server)
      .get(`/employee/${date}`)
      .end(async (err, res) => {
        try {
          res.should.have.status(200);
          res.body.should.be.a('array');
          done();
        } catch (error) {
          done(error)
        }
      })
    });

    it('Création d\'un employé', (done) => {
      const newEmployee = {
        name: 'Doe',
        firstName: 'John',
        dateCreated: '2023-12-15T06:10:00.000Z',
        department: 'IT'
      };

      chai.request(server)
        .post('/employee')
        .send(newEmployee)
        .end(async (err, res) => {
          try {
            res.should.have.status(200);
            res.body.should.be.a('object');
            done();
          } catch (error) {
            done(error);
          }
        });
    });

    it("CheckIn d'un employee", (done) => {
      const employeeId = 3;
      const comment = "C'est l'heure de partir";

      chai.request(server)
      .post('/employee/check_in')
      .send({employeeId, comment})
      .end((err, res) => {
        try {
          res.should.have.status(200);
          res.should.be.an('object');
          done();
        } catch (error) {
          done(error);
        }
      })
    })

    it("CheckOut d'un employee", (done) => {
      const employeeId = 3;
      const comment = "C'est l'heure de partir";
      chai.request(server)
      .post('/employee/check_out')
      .send({employeeId, comment})
      .end((err, res) => {
        try {
          res.should.have.status(200);
          res.should.be.an('object');
          done();
        } catch (error) {
          done(error);
        }
      })
    })
    
  })
})

