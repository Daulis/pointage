const express = require('express');
const CONFIG = require('./app/config/config');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');

const Employee = require('./app/routes/employee');

//Parse l'apllication urlEncode
app.use(bodyParser.urlencoded({extended: false}));

//Parse l'applicatio json
app.use(bodyParser.json({limit: '100mb'}));
app.use(express.json());
app.use(cors());

app.use(function(req, res, next) {
  req.origin_protocol = 'http';
  if (req.protocol === 'http' && req.get('host') && !req.get('host').includes('localhost')) {
        req.origin_protocol = 'https';
    }
    next();
})

app.use('/employee', Employee)

app.get('/', (req, res) => {
  res.status(200).send({message: 'HELLO WELCOME'})
})
app.listen(CONFIG.severvPort, () => console.info(`Server listen in PORT: ${CONFIG.severvPort}...`));
module.exports = app;
