const { insert, findAll, findAllEmployeeWithFilter, checkIn, checkOut } = require("../services/Employee");
const moment = require('moment');

module.exports = {
  /**
   * Controller pour ajouter un emplyoée à la base de donnée
   * 
   * @function createEmployee
   * @async
   * @param {object} req - l'objet de la requette HTTP
   * @param {object} res - l'objet de la reponse HTTP
   * Ajouté l'employée dans la base de donnée et repond avec un message de succes et un status 200
   * En cas d'erreur, renvoie un message d'erreur avec le status 500
   */
  createEmployee: async (req, res) => {
    try{
      const body = req.body;
      await insert(body);
      return res.status(200).json({message: "Employee ajout avec success"});
    } catch (error) {
      return res.status(500).json({
        success: 0,
        message: `Database error connexion ${error}`
      });
    }
  },
  /**
   * Controller pour lister tous les emplyée dans la base de donnée
   * 
   * @function getAllEmployees
   * @async
   * @param {object} res - l'objet de la reponse HTTP
   */
  getAllEmployees: async (req, res) => {
    try {
      await findAll((err, results) => {
        if (err) {
          return res.status(500).json({
            success: 0,
            message: `Database error connection ${err}`
          });
        }
        return res.status(200).send(results);
      });
    } catch (error) {
      return res.status(500).json({
        success: 0,
        message: `Unexpected error: ${error.message}`
      });
    }
    
  },
  /**
   * Controller pour lister un ou plusieurs emplyoée par date
   * 
   * @function getEmployeeByDateCreated
   * @async
   * @param {object} req - l'objet de la requette HTTP
   * @param {object} res - l'objet de la reponse HTTP
   * Ajouté l'employée dans la base de donnée et repond avec un message de succes et un status 200
   * En cas d'erreur, renvoie un message d'erreur avec le status 500
   */
  getEmployeeByDateCreated: async (req, res) => {
    try {
      const {dateCreated} = req.params;
      const formatDate = moment(dateCreated, 'YYYY-MM-DD 00:00:00')
      await findAllEmployeeWithFilter(formatDate, (err, results) => {
          if(err) {
            return res.status(500).json({
            success: 0,
            message: `Database error connection ${err}`
          });
          }
          return res.status(200).send(results);
      })

    } catch (error) {
      console.error('Error fetching employees:', error);
      return res.status(500).json({
        success: 0,
        message: `Unexpected error: ${error.message}`
      });
    }
  },
  /**
   * Controller pour effectuer un CHECKIN d'un employé
   * 
   * @function checkInEmployee
   * @async
   * @param {object} req - l'objet de la requette HTTP
   * @param {object} res - l'objet de la reponse HTTP
   * Ajouté l'employée dans la base de donnée et repond avec un message de succes et un status 200
   * En cas d'erreur, renvoie un message d'erreur avec le status 500
   */
  checkInEmployee: async (req, res) => {
    try {
      const body = req.body;
      await checkIn(body);
      return res.status(200).json({ success: 1, message: 'Check-in effectué avec succès' });
      
    } catch (error) {
      
      console.error('Erreur lors du check-in :', error);
      return res.status(500).json({ success: 0, message: `Erreur de la base de données : ${error.message}` });
    }
  },
  /**
   * Controller pour effectuer un CHECKOUT d'un employé
   * 
   * @function checkOutEmployee
   * @async
   * @param {object} req - l'objet de la requette HTTP
   * @param {object} res - l'objet de la reponse HTTP
   * Ajouté l'employée dans la base de donnée et repond avec un message de succes et un status 200
   * En cas d'erreur, renvoie un message d'erreur avec le status 500
   */
  checkOutEmployee: async (req, res) => {
    try {
      const body = req.body;
      await checkOut(body);
      return res.status(200).json({ success: 1, message: 'Check-out effectué avec succès' });
    } catch (error) {
      return res.status(500).json({ success: 0, message: `Erreur de la base de données : ${error.message}` });
    }
  },
}