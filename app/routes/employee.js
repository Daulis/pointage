const express = require('express');
const { createEmployee, getAllEmployees, getEmployeeByDateCreated, checkInEmployee, checkOutEmployee } = require('../controllers/Employee');
const Router = express.Router();

Router.post('/', createEmployee);
Router.get('/', getAllEmployees);
Router.get('/:dateCreated', getEmployeeByDateCreated);
Router.post('/check_in', checkInEmployee);
Router.post('/check_out', checkOutEmployee);

module.exports = Router;