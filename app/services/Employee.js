const pool = require('../database/database');
const moment = require('moment');

module.exports = {
    /**
   * Insère un nouvel employé dans la base de données.
   * @param {Object} data - Les données de l'employé à insérer.
   * @param {string} data.name - Le nom de famille de l'employé.
   * @param {string} data.firstName - Le prénom de l'employé.
   * @param {string} data.department - Le département dans lequel travaille l'employé.
   * @returns {Promise<Object>} Une promesse qui résoudra avec les résultats de l'opération d'insertion.
   * @reject {Error} Une erreur sera rejetée si l'opération d'insertion échoue.
   */
  insert: (data) => {
    return new Promise((res, reject) => {
      let createdAt = new Date();
      let dbQuery = 'INSERT INTO employees (name, firstName, dateCreated, department) VALUES (?, ?, ?, ?)';
      pool.query(
        dbQuery,
        [
          data.name,
          data.firstName,
          createdAt,
          data.department
        ],
        (error, results, field) => {
          if (error) {
            return reject(error);
          }
          return res(results)
        }
      )

    })
  },
  /**
    * 
    * Récupère tous les employés enregistrés dans la base de données.
    * @param {function} callback - Une fonction de rappel qui sera appelée avec les résultats de la requête.
    * @callback
    * @param {Error|null} error - Une éventuelle erreur survenue lors de l'exécution de la requête.
    * @param {Object[]} results - Les résultats de la requête contenant tous les employés.
   */
  findAll: (callback) => {
    const dbQuery  ='SELECT * FROM employees'
    pool.query(dbQuery, [], (error, results) => {
        if (error) {
            return callback(error);
        }
        return callback(null, results);
      }
    )
  },
  /**
   * Récupère tous les employés enregistrés dans la base de données qui ont été créés à une date spécifique.
   * @param {Date} createdAt - La date à laquelle les employés ont été créés.
   * @param {function} callback - Une fonction de rappel qui sera appelée avec les résultats de la requête.
   * @callback
   * @param {Error|null} error - Une éventuelle erreur survenue lors de l'exécution de la requête.
   * @param {Object[]} results - Les résultats de la requête contenant les employés créés à la date spécifiée.
   */
  findAllEmployeeWithFilter: (createdAt,callback) => {
    const formattedDateTime = moment(createdAt).format("YYYY-MM-DD HH:mm:ss");
    const dbQuery = 'SELECT * FROM employees WHERE DATE(dateCreated) = ?';
    pool.query(dbQuery, [formattedDateTime], (error, results) => {
      if (error) {
          return callback(error);
        }
        return callback(null, results);
      }
    )
  },
  /**
   * Enregistre un événement de pointage (check-in) pour un employé dans la base de données.
   * @param {Object} data - Les données de l'événement de pointage.
   * @param {number} data.employeeId - L'identifiant de l'employé pour lequel l'événement est enregistré.
   * @param {string} data.comment - Un commentaire optionnel associé à l'événement de pointage.
   * @return {Promise} Une promesse qui sera résolue avec les résultats de la requête.
   */
  checkIn: (data) => {
    return new Promise((res, reject) => {
      let date = new Date();
      let dbQuery = 'INSERT INTO check_events (employeeId, eventType, eventTime, comment) VALUES (?, ?, ?, ?)';
      pool.query(dbQuery, [data.employeeId, 'CHECK_IN', date, data.comment],
      (error, results, field) => {
            if (error) {
              return reject(error);
            }
            return res(null,results)
          }
      )
    })
  },
  /**
   * Enregistre un événement de pointage (check-out) pour un employé dans la base de données.
   * Calculera automatiquement la durée entre le check-in précédent et le check-out.
   * @param {Object} data - Les données de l'événement de pointage.
   * @param {number} data.employeeId - L'identifiant de l'employé pour lequel l'événement est enregistré.
   * @param {string} data.comment - Un commentaire optionnel associé à l'événement de pointage.
   * @return {Promise} Une promesse qui sera résolue avec les résultats de la requête.
   */
  checkOut: (data) => {
    return new Promise((res, reject) => {
      let date = new Date();
      
      pool.query('SELECT eventTime FROM check_events WHERE employeeId = ? AND eventType = ?', [data.employeeId, 'CHECK_IN'], (selectError, selectResults) => {
        if (selectError) {
          return reject(selectError);
        }

        if (selectResults.length === 0) {
          return reject(new Error('Aucun check-in trouvé pour cet employé.'));
        }

        const checkInTime = selectResults[0].eventTime;
        const duration = date - checkInTime;
        const hours = Math.floor(duration / 3600000);
        const minutes = Math.floor((duration % 3600000) / 60000);
        const seconds = Math.floor((duration % 60000) / 1000);

        const formattedDuration = `${hours}:${minutes}:${seconds}`;

        pool.query('INSERT INTO check_events (employeeId, eventType, eventTime, comment, duration) VALUES (?, ?, ?, ?, ?)',
          [data.employeeId, 'CHECK_OUT', date, data.comment, formattedDuration],
          (insertError, insertResults) => {
            console.log('DATABASE', insertError);
            if (insertError) {
              return reject(insertError);
            }

            return res(null, insertResults);
          }
        );
      });
    });
  }
}