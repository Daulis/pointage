const mysql = require('mysql2');

const pool = mysql.createPool({
    host: 'mysql_server',
    port: 3306,
    user: 'daulis',
    password: 'root',
    database: 'pointage',
    connectionLimit: 10,
    multipleStatements: true,
    api: {
        allowedOrigin: [
            'http://localhost:4000',
        ]
    }
});

module.exports = pool;
